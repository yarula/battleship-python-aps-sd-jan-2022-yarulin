"""
Entry point for when the game is run as `python -m torpydo`.
"""
from torpydo.battleship import main

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--size', help='size help')
args = parser.parse_args()

DEFAULT_SIZE = 8

size = args.size or DEFAULT_SIZE

print(f"Field size is: {size}")

main(field_size=size)
