import random

import colorama
from colorama import Fore, Back, Style

from torpydo.ship import Color, Letter, Position, Ship
from torpydo.game_controller import GameController

myFleet = []
enemyFleet: list = []

shipsPresets = [
[
['c1','c2','c3','c4','c5'],
['e2','e3','e4','e5'],
['a1','a2','a3'],
['a5','a6','a7'],
['c7','d7']
],
[
['d1','d2','d3','d4','d5'],
['f2','f3','f4','f5'],
['b1','b2','b3'],
['b5','b6','b7'],
['d7','e7']
],
[
['c1','c2','c3','c4','c5'],
['g2','g3','g4','g5'],
['c1','c2','c3'],
['c5','c6','c7'],
['e7','f7']
],
[
['d1','d2','d3','d4','d5'],
['h2','h3','h4','h5'],
['d1','d2','d3'],
['d5','d6','d7'],
['f7','g7']
],
[
['d2','d3','d4','d5','d6'],
['h3','h4','h5','h6'],
['d2','d3','d4'],
['d6','d7','d8'],
['f8','g8']
]
]

def main(field_size=8):
    colorama.init()
    print(Fore.GREEN + r"""
                                    |__
                                    |\/
                                    ---
                                    / | [
                             !      | |||
                           _/|     _/|-++'
                       +  +--|    |--|--|_ |-
                     { /|__|  |/\__|  |--- |||__/
                    +---------------___[}-_===_.'____                 /\
                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _
 __..._____--==/___]_|__|_____________________________[___\==--____,------' .7
|                        Welcome to Battleship                         BB-61/
 \_________________________________________________________________________|""" + Style.RESET_ALL)

    initialize_game()

    start_game()

def print_message(color, s=""):
    print(Fore.YELLOW, color + s + Style.RESET_ALL)

def start_game():
    global myFleet, enemyFleet

    print_message(Fore.YELLOW, r'''
                  __
                 /  \
           .-.  |    |
   *    _.-'  \  \__/
    \.-'       \
   /          _/
   |      _  /
   |     /_\
    \    \_/
     """"""""''')

    def print_hit(color):
        print_message(Fore.YELLOW, color + r'''
            \          .  ./
          \   .:"";'.:..""   /
             (M^^.^~~:.'"").
        -   (/  .    . . \ \)  -
           ((| :. ~ ^  :. .|))
        -   (\- |  \ /  |  /)  -
             -\  \     /  /-
               \  \   /  /''' + Style.RESET_ALL)

    def show_menu():
        flag: bool = False
        while not flag:
            menu_item = input(r"Choose your action: (S)hoot, (E)xit: ").upper()
            if menu_item == 'E':
                import sys
                sys.exit()
            elif menu_item == 'S':
                flag = True
                break
            else:
                print_message(Fore.YELLOW, "Illegal menu command")

    totally_hit_ships = set()

    while True:
        print_message(Fore.YELLOW, )
        print_message(Fore.YELLOW, "Player, it's your turn")

        show_menu()

        if totally_hit_ships:
            print_message(Fore.RED, f"You've already killed: { ','.join([str(x) for x in totally_hit_ships]) }")

        choose = input("\n--------------------------------\nEnter coordinates for your shot:")
        position = parse_position(choose)

        hit_ship = GameController.check_is_hit(enemyFleet, position)

        is_hit = hit_ship is not None

        if hit_ship:
            print_hit(Fore.RED)
            hit_ship.add_hit(position)

            if hit_ship.is_killed():
                totally_hit_ships.add(hit_ship)

        print_message(Fore.YELLOW, "Yeah ! Nice hit !" if is_hit else Fore.CYAN + "Miss" + Style.RESET_ALL)
        position = get_random_position()
        is_hit = GameController.check_is_hit(myFleet, position)
        print_message(Fore.YELLOW, )
        print_message(Fore.YELLOW, f"Computer shoot in {position.column.name}{position.row} and {'hit your ship!' if is_hit else Fore.CYAN + 'miss' + Style.RESET_ALL}")
        if is_hit:
            print_hit(Fore.RED)


def parse_position(input: str):
    letter = Letter[input.upper()[:1]]
    number = int(input[1:])
    position = Position(letter, number)

    return Position(letter, number)

def get_random_position():
    rows = 8
    lines = 8

    letter = Letter(random.randint(1, lines))
    number = random.randint(1, rows)
    position = Position(letter, number)

    return position

def initialize_game():
    initialize_myFleet()

    initialize_enemyFleet()

def initialize_myFleet():
    global myFleet

    myFleet = GameController.initialize_ships()

    print_message(Fore.YELLOW, "Please position your fleet (Game board has size from A to H and 1 to 8) :")

    for ship in myFleet:
        print_message(Fore.YELLOW, )
        print_message(Fore.YELLOW, f"Please enter the positions for the {ship.name} (size: {ship.size})")

        for i in range(ship.size):
            position_input = input(f"Enter position {i} of {ship.size} (i.e A3):")

            ship.add_position(position_input)

def initialize_enemyFleet():
    global enemyFleet


    def get_random_shipsPreset():
        index = random.randint(0, len(shipsPresets) - 1)
        return shipsPresets[index]

    """
    enemyFleet = GameController.initialize_ships()
    randomShipsPreset = get_random_shipsPreset()
    for shipIndex in range(randomShipsPreset.size): 
        positions = randomShipsPreset[shipIndex]
        for positionIndex in range(positions.size): 
            enemyFleet[0]
            
           
    """

    enemyFleet = GameController.initialize_ships()
    randomShipsPreset = get_random_shipsPreset()

    for shipIndex in range(len(randomShipsPreset)):
        positions = randomShipsPreset[shipIndex]
        for positionIndex in range(len(positions)):
            enemyFleet[shipIndex].positions.append(
                parse_position(positions[positionIndex])
            )

    #
    # enemyFleet[0].positions.append(Position(Letter.B, 4))
    # enemyFleet[0].positions.append(Position(Letter.B, 5))
    # enemyFleet[0].positions.append(Position(Letter.B, 6))
    # enemyFleet[0].positions.append(Position(Letter.B, 7))
    # enemyFleet[0].positions.append(Position(Letter.B, 8))
    #
    # enemyFleet[1].positions.append(Position(Letter.E, 6))
    # enemyFleet[1].positions.append(Position(Letter.E, 7))
    # enemyFleet[1].positions.append(Position(Letter.E, 8))
    # enemyFleet[1].positions.append(Position(Letter.E, 9))
    #
    # enemyFleet[2].positions.append(Position(Letter.A, 3))
    # enemyFleet[2].positions.append(Position(Letter.B, 3))
    # enemyFleet[2].positions.append(Position(Letter.C, 3))
    #
    # enemyFleet[3].positions.append(Position(Letter.F, 8))
    # enemyFleet[3].positions.append(Position(Letter.G, 8))
    # enemyFleet[3].positions.append(Position(Letter.H, 8))
    #
    # enemyFleet[4].positions.append(Position(Letter.C, 5))
    # enemyFleet[4].positions.append(Position(Letter.C, 6))

if __name__ == '__main__':
    main()
